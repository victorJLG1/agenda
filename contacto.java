package agenda2;

public class contacto {
   private String nombre,telefono,email;
   private boolean celular;

    public contacto() {
    }

    public contacto(String nombre, String telefono, String email, boolean celular) {
        this.nombre = nombre;
        this.telefono = telefono;
        this.email = email;
        this.celular = celular;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isCelular() {
        return celular;
    }

    public void setCelular(boolean celular) {
        this.celular = celular;
    }

    @Override
    public String toString() {
        return "contacto{" + "nombre=" + nombre + ", telefono=" + telefono + ", email=" + email + ", celular=" + celular + '}';
    }
   
}
